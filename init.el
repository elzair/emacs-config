;;; init.el --- My Emacs init file -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is my personal start-up file for Emacs.

;;; Code:

(require 'package)

; Set package repositories
(setf package-archives '(("gnu"   . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
;; This is a test.

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-file-name-transforms (quote ((".*" "/tmp/" t))))
 '(backup-directory-alist (quote ((".*" . "/tmp/"))))
 '(irony-additional-clang-options (quote ("-std=c++11")))
 '(js-expr-indent-offset 2)
 '(js-indent-level 2)
 '(package-selected-packages
   (quote
    (toml-mode flycheck-rust racer rust-mode rtags glsl-mode circe ggtags markdown-mode flycheck-irony cmake-mode company company-irony company-quickhelp evil ess flycheck irony magit neotree paredit rainbow-delimiters sexp-move solarized-theme yasnippet))))

(package-initialize)

(defun check-packages-and-install-missing-packages ()
  "Ensure that each package in `package-list' is installed.
If a packages is not installed, refresh the package contents
and install the package."
  (interactive)
  (let ((package-contents-refreshed nil))
    (dolist (package package-selected-packages)
      (unless (package-installed-p package)
        ; Ensure package list is up to date
        (when (not package-contents-refreshed)
          (package-refresh-contents)
          (setq package-contents-refreshed t))
        (package-install package)))))

(check-packages-and-install-missing-packages)

(defun init-get-subdirectory (dir-name)
    "Get the full path of Emac's subdirectory DIR-NAME.
This is a clone of the function in scripts/file-sys.el.
It is defined twice so other files can also use it."
    (file-name-as-directory (concat user-emacs-directory
                                    dir-name)))

; Add scripts and rest of config files to `load-path'.
(add-to-list 'load-path
             (init-get-subdirectory "scripts"))
(add-to-list 'load-path
             (init-get-subdirectory "init"))

(require 'vim-emulation)
(require 'gui-config)
(require 'org-config)
(require 'snippets)
(require 'coding-lisp)
(require 'coding-other)
(require 'repl-config)
(require 'version-control)
(require 'file-manager)
(require 'error-checking)

(provide 'init)
;;; init.el ends here

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
