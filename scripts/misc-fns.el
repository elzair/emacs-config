;;; misc-fns.el --- Miscellaneous Functions -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains functions that do not fit anywhere else.

;;; Code:

(require 'evil)

;; Smart Indenting

(defun make-smart-indent ()
  "This function binds the return key to `newline-and-indent' for the current buffer."
    (interactive)
    (define-key evil-insert-state-local-map
      [return]
      'newline-and-indent))


(provide 'misc-fns)
;;; misc-fns.el ends here
