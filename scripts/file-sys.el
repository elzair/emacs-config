;;; file-sys.el --- File System Functions -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains miscellaneous functions for interacting with the file system.

;;; Code:

(defun get-subdirectory (dir-name)
    "Get the full path of Emac's subdirectory DIR-NAME.
This is a clone of the function in init.el.
It is defined twice so other files can also use it."
    (file-name-as-directory (concat user-emacs-directory
                                    dir-name)))

(provide 'file-sys)
;;; file-sys.el ends here
