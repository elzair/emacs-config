;;; file-manager.el --- My File Manager Configuration -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for NEOtree.

;;; Code:

(require 'evil)
(require 'neotree)

(defun my-neotree-mode-hook ()
  "My neotree-mode-hook."
  (define-key evil-normal-state-local-map (kbd "<mouse-1>")
    (lambda (event)
      (interactive "e")
      (mouse-set-point event)
      (neotree-enter)))
  (define-key evil-normal-state-local-map (kbd "TAB") 'neotree-enter)
  (define-key evil-normal-state-local-map (kbd "SPC") 'neotree-enter)
  (define-key evil-normal-state-local-map (kbd "q") 'neotree-hide)
  (define-key evil-normal-state-local-map (kbd "RET") 'neotree-enter)
  (define-key evil-normal-state-local-map (kbd "C") 'neotree-change-root)
  (define-key evil-normal-state-local-map (kbd "R") 'neotree-refresh)
  (define-key evil-normal-state-local-map (kbd "I") 'neotree-hidden-file-toggle)
  (key-chord-define evil-normal-state-local-map "ma" 'neotree-create-node)
  (key-chord-define evil-normal-state-local-map "md" 'neotree-delete-node)
  (key-chord-define evil-normal-state-local-map "mm" 'neotree-rename-node)
  (key-chord-define evil-normal-state-local-map "mc" 'neotree-copy-node))
(add-hook 'neotree-mode-hook 'my-neotree-mode-hook)

(neotree) ; Start file manager on start-up

(provide 'file-manager)
;;; file-manager.el ends here
