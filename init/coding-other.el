;;; coding-other.el --- My Settings for Various Languages -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for various non-Lisp programming languages.

;;; Code:

(require 'ess)
(require 'ess-site)
(require 'flycheck)
(require 'autocompletion)
(require 'misc-config)
(require 'cubescript-mode-load)

;; Add custom style

;; (defconst my-c-style
;;   '((c-comment-only-line-offset . 0)
;; 	(c-hanging-braces-alist     . ((substatement-open after)
;;                                    (brace-list-open)))
;;     (c-hanging-colons-alist     . ((member-init-intro before)
;;                                    (inher-intro)
;;                                    (case-label after)
;;                                    (label after)
;;                                    (access-label after)))
;;     (c-cleanup-list             . (scope-operator
;;                                    empty-defun-braces
;;                                    defun-close-semi))
;;     (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
;;                                    (substatement-open . 0)
;;                                    (case-label        . 4)
;;                                    (block-open        . 0)
;;                                    (knr-argdecl-intro . -))))
;;   "My C/++ Programming Style")

;; (c-add-style "elzair" my-c-style)

(c-add-style "elzair"
              '("bsd"
                (c-basic-offset . 4)))

(setq c-default-style
      '((java-mode . "java")
        (awk-mode  . "awk")
        (other     . "elzair")))

;; Language Specific Hooks

; ESS
(defun my-ess-mode-hook ()
  "My ess-mode-hook."
  ; Disable "shortcut" where pressing "_" yields "->".
  (ess-toggle-S-assign nil))
(add-hook 'ess-mode-hook 'linum-mode)
(add-hook 'ess-mode-hook 'my-ess-mode-hook)

; CMake
(add-hook 'cmake-mode-hook 'linum-mode)
(add-to-list 'auto-mode-alist '("CMakeLists\\.txt" . cmake-mode))
(add-to-list 'auto-mode-alist '("\\.cmake$" . cmake-mode))

; C-style languages hook
(defun my-c-mode-common-hook ()
    "My c-mode-common-hook."
	;(c-set-style "elzair")
    (linum-mode 1)
    (irony-mode)
    (setup-company-mode)
    ;(ggtags-mode 1)
    ;(c-setup-filladapt)
    ;(rtags-start-process-unless-running)
    )
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

; C
(defun my-c-mode-hook ()
	"My c-mode-hook."
	(flycheck-mode 1))
(add-hook 'c-mode-hook 'my-c-mode-hook)

; C++
(defun my-c++-mode-hook ()
    "My `c++-mode-hook'."
    (flycheck-mode 1)
    (setq flycheck-clang-language-standard "c++11"))
(add-hook 'c++-mode-hook 'my-c++-mode-hook)

(add-hook 'python-mode-hook 'linum-mode)

; ECMAScript
(defun my-js-mode-hook ()
    "My `js-mode-hook'." 
    (linum-mode 1)
    (setq-local evil-shift-width 2)
    (setq-local c-basic-offset 2))
(add-hook 'js-mode-hook 'my-js-mode-hook)
(add-hook 'js2-mode-hook 'my-js-mode-hook)

; Rust
(add-hook 'rust-mode-hook #'linum-mode)
(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'rust-mode-hook #'flycheck-mode)

; GLSL
(add-to-list 'auto-mode-alist '("\\.vs$" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.fs$" . glsl-mode))

(provide 'coding-other)
;;; coding-other.el ends here
