;;; vim-emulation.el --- My Evil Mode Configuration -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for Evil Mode.

;;; Code:

(require 'evil)
(require 'epa)
(require 'ess)
(require 'hideshow)
(require 'key-chord)
(require 'neotree)
(require 'paredit)
(require 'coding-other)
(require 'version-control)

;; Evil Commands

(defvar evil-cmds-neotree-pushed-dir
  (concat user-emacs-directory "..")
  "The neotree directory before `evil-cmds-edit-conf' was invoked.")

(defmacro with-beg-and-end (&rest body)
  "This macro defines the beginning and end of a region for the BODY code."
  `(let ((beg (if (> (point) (mark))
                  (mark)
                  (point)))
         (end (if (>= (point) (mark))
                  (point)
                  (mark))))
     ,@body))

(defun evil-cmds-copy ()
  "Copy highlighted text to clipboard."
  (interactive)
  (with-beg-and-end
   (copy-region-as-kill beg end t)))

(defun evil-cmds-cut ()
    "Remove highlighted region and add it to `kill-ring' and clipboard."
    (interactive)
    (with-beg-and-end
     (copy-region-as-kill beg end t)
     (delete-region beg end)))

(defun evil-cmds-edit-conf ()
  "My function for quickly editing Emacs config."
  (interactive)
  (when (not (null (buffer-file-name)))
    (setq evil-cmds-neotree-pushed-dir (file-name-directory (buffer-file-name))))
  (neotree-dir user-emacs-directory)
  (neo-open-file user-init-file))

(defun evil-cmds-save-conf ()
  "My function for saving Emacs config."
  (interactive)
  (if (not (null evil-cmds-neotree-pushed-dir))
      (neotree-dir evil-cmds-neotree-pushed-dir))
  (setq evil-cmds-neotree-pushed-dir nil)
  (kill-buffer "init.el")
  (shell-command "cd ~/.emacs.d && git add -A . && git commit -m 'Updated emacs config' && git pull && git push"))

(evil-mode 1) ; Enable and configure vim emulation
(setq key-chord-two-keys-delay 0.5)

;; Vim Style Mappings

(key-chord-mode 1)
(key-chord-define evil-insert-state-map "ii" 'evil-normal-state)
(key-chord-define evil-insert-state-map "jj" (lambda ()
                                               (interactive)
                                               (right-char 1)))
(define-key evil-visual-state-map "y" 'evil-cmds-copy)
(define-key evil-visual-state-map "d" 'evil-cmds-cut)

;; Vim Style Commands

(evil-ex-define-cmd "ee"   'evil-cmds-edit-conf)
(evil-ex-define-cmd "er"   'eval-region)
(evil-ex-define-cmd "ev"   'eval-expression)
(evil-ex-define-cmd "ff"   'find-function)
(evil-ex-define-cmd "fv"   'find-variable)
(evil-ex-define-cmd "hk"   'describe-key)
(evil-ex-define-cmd "hf"   'describe-function)
(evil-ex-define-cmd "hsha" 'hs-hide-all)
(evil-ex-define-cmd "hshb" 'hs-hide-block)
(evil-ex-define-cmd "hssa" 'hs-show-all)
(evil-ex-define-cmd "hssb" 'hs-show-block)
(evil-ex-define-cmd "hv"   'describe-variable)
(evil-ex-define-cmd "lb"   'list-buffers)
(evil-ex-define-cmd "mc"   'magit-commit)
(evil-ex-define-cmd "ms"   'magit-status)
(evil-ex-define-cmd "pcd"  'paredit-comment-dwim)
(evil-ex-define-cmd "sb"   'switch-to-buffer)
(evil-ex-define-cmd "se"   'evil-cmds-save-conf)

;; Disable Vim Emulation on Certain Buffers

;(evil-set-initial-state 'sly-db-mode 'emacs)
(evil-set-initial-state 'diff-mode         'emacs)
(evil-set-initial-state 'epa-key-list-mode 'emacs)
(evil-set-initial-state 'epa-key-mode      'emacs)
(evil-set-initial-state 'ess-help-mode     'emacs)
(evil-set-initial-state 'term-mode         'emacs)

(provide 'vim-emulation)
;;; vim-emulation.el ends here
