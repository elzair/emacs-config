;;; autocompletion.el --- My Autocompletion Settings -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for company-mode and irony.

;;; Code:

(require 'company)
(require 'irony)
(require 'rtags)
(require 'rust-mode)
;(require 'company-rtags)

;; Helper Functions

(defun setup-company-mode ()
    "This function sets-up `company-mode' and `company-quickhelp-mode' for the given buffer."
    (company-mode 1)
    (company-quickhelp-mode 1))

;; RTags Options

(setq rtags-completions-enabled t)

;; Company Mode Options

(setq company-idle-delay                0.1
      company-tooltip-align-annotations t
      company-dabbrev-downcase          nil)
(add-to-list 'company-backends
             '(company-irony))

;; Irony Initialization

(defun my-irony-mode-hook ()
    "My `irony-mode-hook'.  Replaces `completion-at-point' & `complete-symbol'
functions in `irony-mode' buffers with irony's versions."
    (define-key irony-mode-map
      [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map
      [remap complete-symbol]
      'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;; Racer Options
(add-hook 'racer-mode-hook #'company-mode)
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)

(provide 'autocompletion)
;;; autocompletion.el ends here
