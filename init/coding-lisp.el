;;; coding-lisp.el --- My Settings for Lisp Languages -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for various Lisp programming languages.

;;; Code:

(require 'flycheck)
(require 'paredit)
(require 'rainbow-delimiters)
(require 'sexp-move)
(require 'autocompletion)

;; Common Functions

(defun common-lispy-hooks ()
  "This function provides settings useful for most lisp languages."
    (linum-mode)
    (rainbow-delimiters-mode)
    (enable-paredit-mode)
    (flycheck-mode 1)
    ;; (define-key evil-insert-state-local-map
    ;;   (kbd "<tab>") 'sexp-move-forward)
    ;; (define-key evil-insert-state-local-map
    ;;   (kbd "C-<tab>") 'sexp-move-backward)
    )

;; Language Specific Hooks

; Emacs Lisp
(add-hook 'emacs-lisp-mode-hook 'common-lispy-hooks)
(add-hook 'emacs-lisp-mode-hook 'setup-company-mode)

; Scheme
(add-hook 'scheme-mode-hook 'common-lispy-hooks)

(provide 'coding-lisp)
;;; coding-lisp.el ends here
