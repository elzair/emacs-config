;;; org-config.el --- My Org Configuration -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for Org and Org-Babel.

;;; Code:

(require 'org)
(require 'simple)

;; Helper Functions

(defun org-funcs-preview ()
    "Preview all latex fragments in org buffer."
    (interactive)
    (save-excursion
      (goto-char (point-min))
      (when (not (thing-at-point 'whitespace))
        (open-line 1))
      (org-preview-latex-fragment)))

;; Configuration Options

;; Configure normal org-babel languages.
;; This is currently commented out because it "cannot find ob-sh".
;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((sh . t)
;;    (R  . t)))

(setq org-src-fontify-natively t)          ; Enable syntax highlighting in code blocks
(setq org-format-latex-options
      (plist-put org-format-latex-options
                 ':scale 1.5))             ; Enlarge latex previews

;; Org Mode Hooks

(defun my-org-mode-hook ()
    "My org-mode-hook."
    (visual-line-mode 1)
    (text-scale-adjust 0.5))
(add-hook 'org-mode-hook 'my-org-mode-hook)

(provide 'org-config)
;;; org-config.el ends here
