;;; gui-config.el --- My GUI configuration -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for the presentation layer.

;;; Code:

(set-frame-font "-unknown-Inconsolata LGC-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1")
(set-face-attribute
 'default
 (selected-frame)
 :height 115)                                       ; Increase default font size a bit
(load-theme 'solarized-dark t)
(setq column-number-mode t)                         ; Enable column # display in modeline
(setq inhibit-startup-screen t)                     ; Disable start screen
(scroll-bar-mode -1)                                ; Disable scroll bar
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))                  ; Disable bottom scroll bar
(tool-bar-mode -1)                                  ; Disable tool bar
(setq mouse-autoselect-window t)                    ; Turn on focus follows mouse
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ; Scroll one line at a time
(setq ring-bell-function 'ignore)                   ; Stop bell

;; ; Prevent annoying \"Active processes exist \" query on quit
;; (defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
;;   "Prevent annoying \"Active processes exist\" query when you quit Emacs."
;;   (flet ((process-list ())) ad-do-it))

(provide 'gui-config)
;;; gui-config.el ends here
