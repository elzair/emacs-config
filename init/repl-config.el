;;; repl-config.el --- My REPL Configuration -*- lexical-binding: t -*-

;; Copyright © 2016 Philip Woods

;; Author: Philip Woods <elzairthesorcerer@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains configuration settings for various interactive development systems.

;;; Code:

(require 'vim-emulation)
(require 'misc-fns)
(require 'ess)
(require 'coding-lisp)

;; Common REPL Macros

(defmacro my-repl-eval (repl-eval)
  "This macro will expand to a lambda function that will evalulate the given code by calling the REPL-EVAL function."
         `(lambda ()
            (interactive)
            (progn
              (,repl-eval)
              (evil-insert 0 0))))

(defmacro my-repl-smart (repl-newline-and-indent repl-eval)
  "This macro should evaluate an S-Expression or create a newline.
REPL-NEWLINE-AND-INDENT is the REPLs's equivalent to `newline-and-indent'.
REPL-EVAL is the REPL's function to evaluate an expression."
  `(progn
    (define-key evil-insert-state-local-map
      [return]
      ',repl-newline-and-indent)
    (define-key evil-normal-state-local-map
      [return]
      (my-repl-eval ,repl-eval))))

;; Common Hooks

(defun common-comint-hooks ()
    "This function provides easier history access in comint modes."
  (define-key evil-insert-state-local-map
      (kbd "<up>") 'comint-previous-input)
  (define-key evil-insert-state-local-map
      (kbd "<down>") 'comint-next-input))

;; Program Specific Hooks

; ESS
(defun my-inferior-ess-mode-hook ()
  "My inferior-ess-mode-hook."
  (common-comint-hooks))
(add-hook 'inferior-ess-mode-hook 'my-inferior-ess-mode-hook)

;; ; Scheme REPL
;; (defun my-inferior-scheme-mode-hook ()
;;     "My inferior-scheme-mode-hook."
;;     (rainbow-delimiters-mode)
;;     (enable-paredit-mode))

;; (add-hook 'inferior-scheme-mode-hook 'common-comint-hooks)
;; (add-hook 'inferior-scheme-mode-hook 'my-inferior-scheme-mode-hook)

(provide 'repl-config)
;;; repl-config.el ends here
